package gui;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import core.Capteur;

public class CapteurTableModel extends AbstractTableModel {


	private List<Capteur> capteurs;
	
	public CapteurTableModel(List<Capteur> capteur)
	{
		this.capteurs = capteurs;
	}
	
	public int getColumnCount() {
		// Uniquement 2 colonnes pour ce tableau
		return 3;
	}

	public int getRowCount() {
		
		// Chaque capteur est représenté par une ligne (+1 pour le header)
		return this.capteurs.size() + 1;
	}

	@Override
	public Object getValueAt(int row, int col) {

		// Mise en place des titres 
		
		if(row == 0 && col == 0)
		{
			// Titre colonne nom du capteur
			return this.capteurs.get(row).getNom();
		}
		
		if(row == 0 && col == 1)
		{
			// Titre colonne valeur du capteur
			return "valeur";
		}
		
		if(row == 0 && col == 2)
		{
			// Titre état du capteur
			return "état";
		}
		
		// Le capteur de la ligne correspondante
		Capteur rowCapteur = this.capteurs.get(row);
		
		if (rowCapteur.isRunning())
		{
			// TODO : maj affichage
		}
		else
		{
			// TODO : maj affichage
		}
	}
}
